import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BlogComponent } from './blog/blog.component';
import { VideosComponent } from './media/videos/videos.component';
import { ImagesComponent } from './media/images/images.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'blog',
        component: BlogComponent
    },
    {
        path: 'media/images',
        component: ImagesComponent
    },
    {
        path: 'media/videos',
        component: VideosComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'contact',
        component: ContactComponent
    },
    // when intially visiting website
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    // when path does not match any of the above
    {
        path: '**',
        component: HomeComponent
    }    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}